<%-- 
    Document   : logout
    Created on : Sep 28, 2013, 3:41:29 PM
    Author     : zoya
--%>

<%
session.setAttribute("userid", null);
session.invalidate();
response.sendRedirect("index.jsp");
%>